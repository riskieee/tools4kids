// js

screen.orientation.lock('portrait');

function makeBoxes(howMany) {
  var myElement;
  var boxNode = document.querySelector('.boxes');

  for (var i = 0; i < howMany; i++) {

    myElement = document.createElement('div');
    myElement.className = 'box';
    boxNode.appendChild(myElement);
  }

  boxNode.addEventListener('click', function(e) {
    e.target.classList.toggle('active');
  }, false);

}

makeBoxes(10);
